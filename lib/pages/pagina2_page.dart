import 'package:flutter/material.dart';

class Pagina2Page extends StatelessWidget {
  const Pagina2Page({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Page 2')
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ElevatedButton(
              onPressed: () {},
              child: const Text('Restablecer Usuario', style: TextStyle(color: Colors.white),),
            ),
            ElevatedButton(
              onPressed: () {},
              child: const Text('Cambiar edad', style: TextStyle(color: Colors.white),),
            ),
            ElevatedButton(
              onPressed: () {},
              child: const Text('Agregar Profeciones', style: TextStyle(color: Colors.white),),
            )
          ],
        ),
      )
    );
  }
}