import 'package:flutter/material.dart';
import 'package:flutter_states/pages/pagina1_page.dart';
import 'package:flutter_states/pages/pagina2_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Material App',
      debugShowCheckedModeBanner: false,
      routes: {
        'page1': (context) => Pagina1Page(),
        'page2': (context) => Pagina2Page()
      },
      initialRoute: 'page1',
    );
  }
}